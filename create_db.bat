@echo off
docker stop ibis
docker rm ibis
docker image rm ibis

docker build -f setup/Dockerfile -t ibis .
docker run -it --name ibis --restart unless-stopped --privileged=true -p 50000:50000 -e LICENSE=accept ibis
pause;