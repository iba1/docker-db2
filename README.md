# docker-db2


Run DB2 IBIS database in a Docker container

After install DB will be available on 'localhost:50000'.<br/>
By default, dbname is 'ibis', user is 'userid' and password is 'P@ssw0rd'. It can be changed in 'setup/Dockerfile' file before install.<br/>


Getting started
---------------
1) Download and install Docker Desktop from https://hub.docker.com/editions/community/docker-ce-desktop-windows/
2) Clone master branch
3) Copy DB files (db2move.lst, tab1.ixf, tab2.ixf etc.) for import into 'setup/db' folder (you can find DB dumps at \\\ewa7.itd.iba.by\sbsa\\==DBz==\)
4) Optional: Copy SQL scripts into 'setup/sql_scripts' folder (scripts will be executed after import will be finished, update user's centr for example)
5) Run 'create_db.bat' (can take up to 10 minutes)

After DB was created, you can launch it from 'start_db.bat' and stop via 'stop_db.bat'. You can also use Docker commands 'docker start ibis' or 'docker stop ibis' as well