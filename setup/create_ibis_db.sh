#!/bin/bash
chown -R ${DB2INSTANCE} /opt/setup/;
cd /opt/setup/db;

echo -e "\033[32m\nRestarting DB daemon\033[0m";
su ${DB2INSTANCE} -c "db2set db2codepage=437"
su ${DB2INSTANCE} -c "db2set db2country=1"
su ${DB2INSTANCE} -c "db2 terminate"
su ${DB2INSTANCE} -c "db2stop"
su ${DB2INSTANCE} -c "db2start"

echo -e "\033[32m\nCreating DB... can take a few minutes!\033[0m";
su ${DB2INSTANCE} -c "db2 create db ${DB_NAME} USING CODESET IBM-437 TERRITORY us"
su ${DB2INSTANCE} -c "db2 connect to ${DB_NAME}"
su ${DB2INSTANCE} -c "db2 grant dbadm on database to user ${DB2INSTANCE}"

echo -e "\033[32m\nImporting DB from db folder:\033[0m";
su ${DB2INSTANCE} -c "db2move ${DB_NAME} import -u ${DB2INSTANCE} -p ${DB2INST1_PASSWORD}"

echo -e "\033[32m\nRunning SQL scripts\033[0m";
for i in `find /opt/setup/sql_scripts/ -type f`; do su ${DB2INSTANCE} -c "db2 connect to ${DB_NAME}; db2 -tvmf $i"; done
while :; do echo -e "\033[32m\nInstallation finished! Press CTRL+C and then (CTRL+P+Q to close window or CTRL+C to stop container)\033[0m"; sleep 60; done